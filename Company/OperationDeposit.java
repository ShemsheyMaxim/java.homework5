package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public interface OperationDeposit {
    double getDeposit();

    default double getDeposit(int depositAmount, int countOfMonthForInvestment, double rate, String typeDeposit) {
        double countOfPeriodsOfCapitalizationPerYear = 0;
        if (typeDeposit.equalsIgnoreCase("с выплатой в конце срока")) {
            countOfPeriodsOfCapitalizationPerYear = 1;
//            return depositAmount * (1 + ((rate*0.01)/12))* countOfMonthForInvestment;
        } else if (typeDeposit.equalsIgnoreCase("с ежемесячной капитализацией")) {
            countOfPeriodsOfCapitalizationPerYear = 12;
        } else if (typeDeposit.equalsIgnoreCase("с ежедневной капитализацией")) {
            countOfPeriodsOfCapitalizationPerYear = 365;
        } else if (typeDeposit.equalsIgnoreCase("с ежеквартальной капитализацией")) {
            countOfPeriodsOfCapitalizationPerYear = 4;
        } else {
            return -1;
//            System.out.println("Тип депизит может быть: 1)с выплатой в конце срока 2)с ежедневной капитализацией 3)с ежемесячной капитализацией 4)с ежеквартальной капитализацией");
//            System.exit(0);
        }

//        переменная создана для того,чтобы в дальнейшем переработать метод и получать не конечную процентную ставку в зависисмости от типа кредита, а конечную сумму от кредита.
//        данная реализация будет позволять сравнивать эффективную ставку для депозита,а не простую ставку указанную в обьекте
//        сравнивая одну и тужу ставку обькта она может совпадать,но в зависимости от типа мы будем иметь различную эффективную ставку,которую получим по итогу вложения по данному типу депозита
        double effectiveRate = (Math.pow(1 + (rate / (100f * countOfPeriodsOfCapitalizationPerYear)), (countOfPeriodsOfCapitalizationPerYear * (countOfMonthForInvestment / 12f))) - 1) * (100 / (countOfMonthForInvestment / 12f));

        return effectiveRate;
    }


}
