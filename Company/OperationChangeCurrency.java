package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public interface OperationChangeCurrency {
    String UAH = "UAH";
    String EUR = "EUR";
    String USD = "USD";
    String RUB = "RUB";

    double changeCurrency();

    default double convertUAHtoUSD(int exchangeAmount, double courseUSD) {
        return exchangeAmount / courseUSD;
    }

    default double convertUAHtoEUR(int exchangeAmount, double courseEUR) {
        return exchangeAmount / courseEUR;
    }

    default double convertUSDtoUAH(int exchangeAmount, double courseUSD) {
        return exchangeAmount * courseUSD;
    }

    default double convertEURtoUAH(int exchangeAmount, double courseEUR) {
        return exchangeAmount * courseEUR;
    }

    default double convertUAHtoRUB(int exchangeAmount, double courseRUB) {
        return exchangeAmount / courseRUB;
    }

    default double convertRUBtoUAH(int exchangeAmount, double courseRUB) {
        return exchangeAmount * courseRUB;
    }
}
