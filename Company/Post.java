package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public class Post extends Company implements OperationSendMoney {
    private final int amountMoney;
    private final int commissionInPercentForSending;
    private final int mandatoryCommission;

    public Post(String name, String address, int amountMoney, int commissionInPercentForSending, int mandatoryCommission) {
        super(name, address);
        this.amountMoney = amountMoney;
        this.commissionInPercentForSending = commissionInPercentForSending;
        this.mandatoryCommission = mandatoryCommission;
    }

    public int getAmountMoney() {
        return amountMoney;
    }

    public int getCommissionInPercentForSending() {
        return commissionInPercentForSending;
    }

    public int getMandatoryCommission() {
        return mandatoryCommission;
    }

    @Override
    public double sendMoney() {
        return commissionForSendMoney(amountMoney, commissionInPercentForSending, mandatoryCommission);
    }

    @Override
    public String toString() {
        return "Название: '" + getName() + '\'' +
                ", адресс: " + getAddress() +
                ", сумма денег для пересылки = " + amountMoney + " грн" +
                ", процентная комиссия = " + commissionInPercentForSending + '%' +
                ", обязательная комиссия = " + mandatoryCommission + " грн.";
    }
}
