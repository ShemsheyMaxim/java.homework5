package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public class Bank extends Company implements OperationChangeCurrency, OperationCredit, OperationDeposit, OperationSendMoney {
    private final int yearOfGetLicense;
    private final double courseUSD;
    private final double courseEUR;
    private final double courseRUB;
    private final int commissionForConvert;
    private final int maxAmountToConvert;
    private final int exchangeAmount;
    private String currency;
    private String currencyForExchange;
    private final int maxCredit;
    private final int percentPerAnnum;
    private final int amountForCredit;
    private final int countMonthForCredit;
    private String typeCredit;
    private final int depositAmount;
    private final int countOfMonthForInvestment;
    private final double rate;
    private String typeDeposit;
    private final int amountMoney;
    private final int commissionInPercentForSending;
    private final int mandatoryCommission;

    public Bank(String name, String headOfficeAddress, int yearOfGetLicense, double courseUSD, double courseEUR,
                double courseRUB, int exchangeAmount, String currency, String currencyForExchange,
                int commissionForConvert, int maxAmountToConvert, int maxCredit, int percentPerAnnum,
                int amountForCredit, int countMonthForCredit, String typeCredit, int depositAmount,
                int countOfMonthForInvestment, double rate, String typeDeposit, int amountMoney, int commissionInPercentForSending,
                int mandatoryCommission) {
        super(name, headOfficeAddress);
        this.yearOfGetLicense = yearOfGetLicense;
        this.courseUSD = courseUSD;
        this.courseEUR = courseEUR;
        this.courseRUB = courseRUB;
        this.exchangeAmount = exchangeAmount;
        this.currency = currency;
        this.currencyForExchange = currencyForExchange;
        this.commissionForConvert = commissionForConvert;
        this.maxAmountToConvert = maxAmountToConvert;
        this.maxCredit = maxCredit;
        this.percentPerAnnum = percentPerAnnum;
        this.amountForCredit = amountForCredit;
        this.countMonthForCredit = countMonthForCredit;
        this.typeCredit = typeCredit;
        this.depositAmount = depositAmount;
        this.countOfMonthForInvestment = countOfMonthForInvestment;
        this.rate = rate;
        this.typeDeposit = typeDeposit;
        this.amountMoney = amountMoney;
        this.commissionInPercentForSending = commissionInPercentForSending;
        this.mandatoryCommission = mandatoryCommission;
    }

    public int getYearOfGetLicense() {
        return yearOfGetLicense;
    }

    public double getCourseUSD() {
        return courseUSD;
    }

    public double getCourseEUR() {
        return courseEUR;
    }

    public double getCourseRUB() {
        return courseRUB;
    }

    public int getCommissionForConvert() {
        return commissionForConvert;
    }

    public int getMaxAmountToConvert() {
        return maxAmountToConvert;
    }

    public int getExchangeAmount() {
        return exchangeAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencyForExchange() {
        return currencyForExchange;
    }

    public String getUAH() {
        return UAH;
    }

    public String getEUR() {
        return EUR;
    }

    public String getUSD() {
        return USD;
    }

    public String getRUB() {
        return RUB;
    }

    public int getMaxCredit() {
        return maxCredit;
    }

    public int getPercentPerAnnum() {
        return percentPerAnnum;
    }

    public int getAmountForCredit() {
        return amountForCredit;
    }

    public int getCountMonthForCredit() {
        return countMonthForCredit;
    }

    public String getTypeCredit() {
        return typeCredit;
    }

    public int getDepositAmount() {
        return depositAmount;
    }

    public int getCountOfMonthForInvestment() {
        return countOfMonthForInvestment;
    }

    public double getRate() {
        return rate;
    }

    public String getTypeDeposit() {
        return typeDeposit;
    }

    public int getAmountMoney() {
        return amountMoney;
    }

    public int getCommissionInPercentForSending() {
        return commissionInPercentForSending;
    }

    public int getMandatoryCommission() {
        return mandatoryCommission;
    }

    @Override
    public double convertUAHtoUSD(int exchangeAmount, double courseUSD) {
        return exchangeAmount <= maxAmountToConvert ? (exchangeAmount - commissionForConvert) / courseUSD : -1;
//            System.out.println("Банк не может обменять больше 12 000 грн.");
    }

    @Override
    public double convertUAHtoEUR(int exchangeAmount, double courseEUR) {
        return (exchangeAmount - commissionForConvert) <= maxAmountToConvert ? exchangeAmount / courseEUR : -1;
//            System.out.println("Банк не может обменять больше 12 000 грн.");
    }

    @Override
    public double convertUAHtoRUB(int exchangeAmount, double courseRUB) {
        return (exchangeAmount - commissionForConvert) <= maxAmountToConvert ? exchangeAmount / courseRUB : -1;
//            System.out.println("Банк не может обменять больше 12 000 грн.");
    }

    @Override
    public double convertUSDtoUAH(int exchangeAmount, double courseUSD) {
        return countMaxConvertAmountUSDtoUAH(courseUSD) <= exchangeAmount ? (exchangeAmount / courseUSD) - commissionForConvert : -1;
//            System.out.println(String.format("Банк не может обменять больше %.2f USD", countMaxConvertAmountUSDtoUAH(exchangeAmount,courseUSD)));
    }

    private double countMaxConvertAmountUSDtoUAH(double courseUSD) {
        return maxAmountToConvert / courseUSD;
    }

    @Override
    public double convertEURtoUAH(int exchangeAmount, double courseEUR) {
        return countMaxConvertAmountEURtoUAH(courseEUR) <= exchangeAmount ? (exchangeAmount / courseEUR) - commissionForConvert : -1;
//            System.out.println(String.format("Банк не может обменять больше %.2f USD", countMaxConvertAmountEURtoUAH(exchangeAmount,courseEUR) ));
    }

    private double countMaxConvertAmountEURtoUAH(double courseEUR) {
        return maxAmountToConvert / courseEUR;
    }

    @Override
    public double convertRUBtoUAH(int exchangeAmount, double courseRUB) {
        return countMaxConvertAmountRUBtoUAH(courseRUB) <= exchangeAmount ? (exchangeAmount / courseRUB) - commissionForConvert : -1;
//            System.out.println(String.format("Банк не может обменять больше %.2f USD", countMaxConvertAmountRUBtoUAH(courseRUB)));
    }

    private double countMaxConvertAmountRUBtoUAH(double courseRUB) {
        return maxAmountToConvert / courseRUB;
    }

    @Override
    public double changeCurrency() {
        if (currencyForExchange.equalsIgnoreCase(USD)) {
            return convertUAHtoUSD(exchangeAmount, courseUSD);
        } else if (currencyForExchange.equalsIgnoreCase(EUR)) {
            return convertUAHtoEUR(exchangeAmount, courseEUR);
        } else if (currencyForExchange.equalsIgnoreCase(RUB)) {
            return convertUAHtoRUB(exchangeAmount, courseRUB);
        } else if (currencyForExchange.equalsIgnoreCase(UAH)) {
            if (currency.equalsIgnoreCase(USD)) {
                return courseUSD * exchangeAmount;
            } else if (currency.equalsIgnoreCase(EUR)) {
                return courseEUR * exchangeAmount;
            } else if (currency.equalsIgnoreCase(RUB)) {
                return courseRUB * exchangeAmount;
            }
//                System.out.println("Доступные валюты для обмена: USD,EUR,UAH,RUB");
//                System.exit(0);
        }
        return -1;
    }

    @Override
    public double giveCredit() {
        if (typeCredit.equalsIgnoreCase("Annuity")) {
            return giveCreditForAnnuity(maxCredit, amountForCredit, percentPerAnnum, countMonthForCredit);
        } else if (getTypeCredit().equalsIgnoreCase("Differentiated")) {
            return giveCreditForDifferentiated(maxCredit, amountForCredit, percentPerAnnum, countMonthForCredit);
        } else {
            return -1;
//            System.out.println("Тип кредита может быть: Annuity или Differentiated");
//            System.exit(0);
        }
    }

    @Override
    public double getDeposit() {
        return countOfMonthForInvestment <= 12 ? getDeposit(depositAmount, countOfMonthForInvestment, rate, typeDeposit) : -1;
//            System.out.println("Банк может принимать депозит на срок до 12 месяцев.");
//            System.exit(0);
    }

    @Override
    public double sendMoney() {
        return commissionForSendMoney(amountForCredit, commissionInPercentForSending, mandatoryCommission);
    }

    @Override
    public String toString() {
        return "Название: '" + getName() + '\'' +
                ", адресс: " + getAddress() +
                ", год получения лицензии = " + yearOfGetLicense +
                ",  курс USD = " + courseUSD + " грн" +
                ", курс EUR = " + courseEUR + " грн" +
                ", курс RUB = " + courseRUB + " грн" +
                ", коммиссия при конвертации = " + commissionForConvert + " грн" +
                ", максимальная сумма конвертации = " + maxAmountToConvert + " грн" +
                ", обмениваемая сумма =" + exchangeAmount + " грн" +
                ", обмениваем '" + currency + '\'' +
                " в '" + currencyForExchange + '\'' +
                ", максимальный кредит = " + maxCredit + " грн" +
                ", процентная ставка = " + percentPerAnnum + '%' +
                ", сумма для кредита = " + amountForCredit + " грн" +
                ", период кредита = " + countMonthForCredit + " месяцев" +
                ", тип кредита '" + typeCredit + '\'' +
                ", сумма депозита = " + depositAmount + " грн" +
                ", период депозита = " + countOfMonthForInvestment + " месяцев" +
                ", ставка = " + rate + '%' +
                ", тип депозита '" + typeDeposit + '\'' +
                ", сумма денег для пересылки = " + amountMoney + " грн" +
                ", процентная комиссия = " + commissionInPercentForSending + '%' +
                ", обязательная комиссия = " + mandatoryCommission + " грн.";
    }
}
