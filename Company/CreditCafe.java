package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public class CreditCafe extends CompanyForCredit {

    public CreditCafe(String name, String address, int maxCredit, int percentPerAnnum, int amountForCredit, int countMonthForCredit, String typeCredit) {
        super(name, address, maxCredit, percentPerAnnum, amountForCredit, countMonthForCredit, typeCredit);
    }
}
