package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public abstract class Company {
    private String name;
    private String address;

    Company(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "Компания:" +
                " название '" + name + '\'' +
                ", адресс " + address;
    }
}
