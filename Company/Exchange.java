package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public class Exchange extends Company implements OperationChangeCurrency {
    private final double courseUSD;
    private final double courseEUR;
    private final int exchangeAmount;
    private String currency;
    private String currencyForExchange;

    public Exchange(String name, String address, String currency, int exchangeAmount, String currencyForExchange,
                    double courseUSD, double courseEUR) {
        super(name, address);
        this.exchangeAmount = exchangeAmount;
        this.currency = currency;
        this.currencyForExchange = currencyForExchange;
        this.courseEUR = courseEUR;
        this.courseUSD = courseUSD;
    }

    public double getCourseUSD() {
        return courseUSD;
    }

    public double getCourseEUR() {
        return courseEUR;
    }

    public int getExchangeAmount() {
        return exchangeAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencyForExchange() {
        return currencyForExchange;
    }

    @Override
    public double changeCurrency() {
        if (currencyForExchange.equalsIgnoreCase(USD)) {
            return convertUAHtoUSD(exchangeAmount, courseUSD);
        } else if (currencyForExchange.equalsIgnoreCase(EUR)) {
            return convertUAHtoEUR(exchangeAmount, courseEUR);
        } else if (currencyForExchange.equalsIgnoreCase(UAH)) {
            if (currency.equalsIgnoreCase(USD)) {
                return courseUSD * exchangeAmount;
            } else if (currency.equalsIgnoreCase(EUR)) {
                return courseEUR * exchangeAmount;
            }
        } else {
            return -1;
//            System.out.println("Доступные валюты для обмена: USD,EUR,UAH,RUB");
//            System.exit(0);
        }
        return -1;
    }

    @Override
    public String toString() {
        return "Название: '" + getName() + '\'' +
                ", адресс: " + getAddress() +
                ", курс USD = " + courseUSD +
                ", курс EUR = " + courseEUR +
                ", обмениваемая сумма = " + exchangeAmount + "грн" +
                ", обмениваем '" + currency + '\'' +
                " в '" + currencyForExchange + '\'' + ".";
    }
}
