package Company;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public interface OperationCredit {
    double giveCredit();

    default double giveCreditForAnnuity(int maxCredit, int amountForCredit, int percentPerAnnum, int countMonthForCredit) {
        if (maxCredit >= amountForCredit) {
            double interestRatePerMonth = (percentPerAnnum * 0.01) / 12f;
            double payment = (amountForCredit * interestRatePerMonth) / (1 - Math.pow(1 + interestRatePerMonth, -countMonthForCredit));
            return payment * countMonthForCredit;
        } else {
            return -1;
//            System.out.println(String.format("Максимальная сумма кредита ломбарда %d грн", maxCredit));
//            System.exit(0);
        }
    }

    default double giveCreditForDifferentiated(int maxCredit, int amountForCredit, int percentPerAnnum, int countMonthForCredit) {
        if (maxCredit >= amountForCredit) {
            double basicMonthlyPayment = amountForCredit / (float) countMonthForCredit;
            double amountOfRemainingForCredit = amountForCredit;
            double amountPercentForCredit = 0;
            for (int countOfRemainingMonths = 0; countOfRemainingMonths < countMonthForCredit; countOfRemainingMonths++) {
                double payment = (basicMonthlyPayment) + (amountOfRemainingForCredit * (percentPerAnnum * 0.01) / 12);
                amountOfRemainingForCredit = amountOfRemainingForCredit - basicMonthlyPayment;
                amountPercentForCredit = amountPercentForCredit + (payment - basicMonthlyPayment);
            }
            return amountForCredit + amountPercentForCredit;
        } else {
            return -1;
//            System.out.println(String.format("Максимальная сумма кредита ломбарда %d грн", maxCredit));
//            System.exit(0);
        }
    }
}
