package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public interface OperationSendMoney {
    double sendMoney();

    default double commissionForSendMoney(int amountMoney, int commissionInPercent, int mandatoryCommission) {
        return commissionInPercent != 0 ? amountMoney * (commissionInPercent * 0.01) + mandatoryCommission : mandatoryCommission;
    }
}
