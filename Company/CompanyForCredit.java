package Company;

/**
 * Created by Maxim Shemshey on 27.05.2020.
 */
public abstract class CompanyForCredit extends Company implements OperationCredit {
    private final int maxCredit;
    private final int percentPerAnnum;
    private final int amountForCredit;
    private final int countMonthForCredit;
    private String typeCredit;

    CompanyForCredit(String name, String address, int maxCredit, int percentPerAnnum, int amountForCredit, int countMonthForCredit, String typeCredit) {
        super(name, address);
        this.maxCredit = maxCredit;
        this.percentPerAnnum = percentPerAnnum;
        this.amountForCredit = amountForCredit;
        this.countMonthForCredit = countMonthForCredit;
        this.typeCredit = typeCredit;
    }

    public int getMaxCredit() {
        return maxCredit;
    }

    public int getPercentPerAnnum() {
        return percentPerAnnum;
    }

    public int getAmountForCredit() {
        return amountForCredit;
    }

    public int getCountMonthForCredit() {
        return countMonthForCredit;
    }

    public String getTypeCredit() {
        return typeCredit;
    }

    @Override
    public double giveCredit() {
        if (typeCredit.equalsIgnoreCase("Annuity")) {
            return giveCreditForAnnuity(maxCredit, amountForCredit, percentPerAnnum, countMonthForCredit);
        } else if (typeCredit.equalsIgnoreCase("Differentiated")) {
            return giveCreditForDifferentiated(maxCredit, amountForCredit, percentPerAnnum, countMonthForCredit);
        }
//            System.out.println("Тип кредита может быть: Annuity или Differentiated");
//            System.exit(0);
        return -1;
    }

    @Override
    public String toString() {
        return "Название: '" + getName() + '\'' +
                ", адресс: " + getAddress() +
                ", максимальный кредит = " + maxCredit + "грн" +
                ", процентная ставка = " + percentPerAnnum + '%' +
                ", сумма кредита = " + amountForCredit + "грн" +
                ", период кредита = " + countMonthForCredit + " месяцев" +
                ", тип кредита '" + typeCredit + '\'' + ".";
    }
}
