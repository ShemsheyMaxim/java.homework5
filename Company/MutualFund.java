package Company;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public class MutualFund extends Company implements OperationDeposit {
    private final int yearOfFoundation;
    private final int depositAmount;
    private final int countOfMonthForInvestment;
    private final double rate;
    private String typeDeposit;

    public MutualFund(String name, String address, int yearOfFoundation, int depositAmount, int countOfMonthForInvestment, double rate, String typeDeposit) {
        super(name, address);
        this.yearOfFoundation = yearOfFoundation;
        this.depositAmount = depositAmount;
        this.countOfMonthForInvestment = countOfMonthForInvestment;
        this.rate = rate;
        this.typeDeposit = typeDeposit;
    }

    public int getYearOfFoundation() {
        return yearOfFoundation;
    }

    public int getDepositAmount() {
        return depositAmount;
    }

    public int getCountOfMonthForInvestment() {
        return countOfMonthForInvestment;
    }

    public double getRate() {
        return rate;
    }

    public String getTypeDeposit() {
        return typeDeposit;
    }

    @Override
    public double getDeposit() {
        return countOfMonthForInvestment >= 12 ? getDeposit(depositAmount, countOfMonthForInvestment, rate, typeDeposit) : -1;
//            System.out.println("ПИФ может принимать депозит на срок от 12 месяцев.");
//            System.exit(0);
    }

    @Override
    public String toString() {
        return "Название: '" + getName() + '\'' +
                ", адресс: " + getAddress() +
                ", год основания = " + yearOfFoundation +
                ", сумма депозита = " + depositAmount + "грн" +
                ", период депозита = " + countOfMonthForInvestment + " месяцев" +
                ", ставка = " + rate + '%' +
                ", тип депозита '" + typeDeposit + '\'' + ".";
    }
}
