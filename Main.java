import Company.*;

/**
 * Created by Maxim Shemshey on 26.05.2020.
 */
public class Main {
    private static Company[] company;

    public static void main(String[] args) {
        initCompany();
        printTheBestCompanyForConversation(company);
        printCompanyForTheBestCredit(company);
        printTheBestCompanyForDeposit(company);
        printTheBestCompanyForSendingMoney(company);

//        System.out.println(String.format("Взяв %d грн в кредит под %d общая сумма к выплате будет составлять: %.2f грн.", pawnshop.getAmountForCredit(), pawnshop.getPercentPerAnnum(), pawnshop.giveCredit()));
//        System.out.println(String.format("Положив %d грн в %s на депозит под %.2f ваша эффективная ставка будет составлять %.3f ", mutualFund.getDepositAmount(), mutualFund.getName(), mutualFund.getRate(), mutualFund.getDeposit()));
    }

    private static Company[] initCompany() {
        company = new Company[7];

        company[0] = new Bank("Bank", "ул.Клочковская", 2000,
                26.8, 28.4, 0.34, 20_000, "UAH",
                "USD", 15, 12_000,
                200_000, 25, 50_000, 12,
                "Differentiated", 100000, 12, 20,
                "с выплатой в конце срока", 1000, 1, 5);
        company[1] = new Exchange("Exchange", "ул.Гагарина", "UAH",
                1000, "USD", 26.9, 28.2);
        company[2] = new Pawnshop("Pawnshop", "ул. Шевченка", 50_000,
                40, 50_000, 12, "Differentiated");
        company[3] = new CreditCafe("CreditCafe", "ул. Сагайдачного", 4_000,
                200, 50_000, 12, "Differentiated");
        company[4] = new CreditUnion("CreditUnion", "ул. Гарибальди", 100_000,
                20, 50_000, 12, "Differentiated");
        company[5] = new MutualFund("MutualFund", "ул. Гвардейцев", 1997,
                100000, 12, 20, "с ежемесячной капитализацией");
        company[6] = new Post("Post", "ул.Контимировцев", 1000, 2,
                0);

        return company;
    }

    private static Company[] searchTheBestCompaniesToConversation(Company[] company) {

        Company theBestCompanyToConversationInUSD = null;
        Company theBestCompanyToConversationInEUR = null;
        Company theBestCompanyToConversationInRUB = null;
        double theBestCourseToConversationUSD = Double.MAX_VALUE;
        double theBestCourseToConversationEUR = Double.MAX_VALUE;
        double theBestCourseToConversationRUB = Double.MAX_VALUE;
        for (Company companyToConversation : company) {
            if (companyToConversation instanceof Exchange) {
                if (((Exchange) companyToConversation).getCourseUSD() < theBestCourseToConversationUSD) {
                    theBestCourseToConversationUSD = ((Exchange) companyToConversation).getCourseUSD();
                    theBestCompanyToConversationInUSD = companyToConversation;
                }
                if (((Exchange) companyToConversation).getCourseEUR() < theBestCourseToConversationEUR) {
                    theBestCourseToConversationEUR = ((Exchange) companyToConversation).getCourseEUR();
                    theBestCompanyToConversationInEUR = companyToConversation;
                }
            } else if (companyToConversation instanceof Bank) {
                if (((Bank) companyToConversation).getMaxAmountToConvert() >= ((Bank) companyToConversation).getExchangeAmount()) {
                    if (((Bank) companyToConversation).getCourseUSD() < theBestCourseToConversationUSD) {
                        theBestCourseToConversationUSD = ((Bank) companyToConversation).getCourseUSD();
                        theBestCompanyToConversationInUSD = companyToConversation;
                    }
                    if (((Bank) companyToConversation).getCourseEUR() < theBestCourseToConversationEUR) {
                        theBestCourseToConversationEUR = ((Bank) companyToConversation).getCourseEUR();
                        theBestCompanyToConversationInEUR = companyToConversation;
                    }
                    if (((Bank) companyToConversation).getCourseRUB() < theBestCourseToConversationRUB) {
                        theBestCourseToConversationRUB = ((Bank) companyToConversation).getCourseRUB();
                        theBestCompanyToConversationInRUB = companyToConversation;
                    }
                } else {
                    // я не уверен нужно ли вывыдоить что данная компания не может обменять данную сумму
                    System.out.println(String.format("%s не может обменять больше %d грн", companyToConversation.getName(), ((Bank) companyToConversation).getMaxAmountToConvert()));
                }
            }
        }
        return new Company[]{theBestCompanyToConversationInUSD, theBestCompanyToConversationInEUR, theBestCompanyToConversationInRUB};
    }

    private static void printTheBestCompanyForConversation(Company[] company) {
        Company[] companies = searchTheBestCompaniesToConversation(company);
        /*
        //я думаю так было бы правильно,но я не понимаю как достучаться в else,
        я понимаю что я сохранял их как компании и там нет таких методов,
        а так как они нулл и привести к другому виду не могу и получать такие методы так же не могу
        for (Company company : companies) {
            if (company != null) {
                if (company instanceof Bank) {
                    System.out.println(String.format("Лучшей компание для обмена %s является: %s", ((Bank) company).getCurrencyForExchange(), company.getName()));
                    System.out.println(company.toString());
                } else if (company instanceof Exchange) {
                    System.out.println(String.format("Лучшей компание для обмена %s является: %s", ((Exchange) company).getCurrencyForExchange(), company.getName()));
                    System.out.println(company.toString());
                }
            } else {
                System.out.println(String.format("Лучшей компанией для обмена %d UAH на %s не найдено",company.getMaxAmountToConvert, company.getCurrencyForExchange()));
            }
        }
        */
        if (companies[0] != null) {
            System.out.println(String.format("Лучшей компание для обмена USD является: %s", companies[0].getName()));
            System.out.println(companies[0].toString());
        } else {
            System.out.println("Лучшей компанией для обмена данной суммы на USD не найдено");
        }
        if (companies[1] != null) {
            System.out.println(String.format("Лучшей команией для обмена EUR является: %s", companies[1].getName()));
            System.out.println(companies[1].toString());
        } else {
            System.out.println("Лучшей компанией для обмена данной суммы на EUR не найдено");
        }
        if (companies[2] != null) {
            System.out.println(String.format("Лучшей команией для обмена RUB является: %s", companies[2].getName()));
            System.out.println(companies[2].toString());
        } else {
            System.out.println("Лучшей компанией для обмена данной суммы на RUB не найдено");
        }
    }

    private static Company researchCompanyForTheBestCredit(Company[] company) {
        Company theBestCompanyForCredit = null;
        double theBestPercent = Double.MAX_VALUE;
        for (Company companyForCredit : company) {
            if (companyForCredit instanceof CompanyForCredit) {
                if (((CompanyForCredit) companyForCredit).getAmountForCredit() <= ((CompanyForCredit) companyForCredit).getMaxCredit()) {
                    if (theBestPercent > ((CompanyForCredit) companyForCredit).giveCredit()) {
                        theBestCompanyForCredit = companyForCredit;
                    }
                }
            }
        }
        return theBestCompanyForCredit;
    }

    private static void printCompanyForTheBestCredit(Company[] company) {
        Company theBestCompanyForCredit = researchCompanyForTheBestCredit(company);
        if (theBestCompanyForCredit != null) {
            System.out.println(String.format("Лучшей компанией для кредита: %s", theBestCompanyForCredit.getName()));
            System.out.println(theBestCompanyForCredit.toString());
        } else {
            System.out.println("Лучшей компанией для кредита не найдено");
        }
    }

    private static Company researchTheBestCompanyForDeposit(Company[] company) {
        Company theBestCompanyForDeposit = null;
        double theBestPercentForDeposit = 0;
        for (Company companyForDeposit : company) {
            if (companyForDeposit instanceof OperationDeposit) {
                if (((OperationDeposit) companyForDeposit).getDeposit() > theBestPercentForDeposit) {
                    theBestPercentForDeposit = ((OperationDeposit) companyForDeposit).getDeposit();
                    theBestCompanyForDeposit = companyForDeposit;
                }
            }
        }
        return theBestCompanyForDeposit;
    }

    private static void printTheBestCompanyForDeposit(Company[] company) {
        Company theBestCompanyForDeposit = researchTheBestCompanyForDeposit(company);
        if (theBestCompanyForDeposit != null) {
            System.out.println(String.format("Лучшей компанией для депозита на данный срок являеться: \"%s\" ", theBestCompanyForDeposit.getName()));
            System.out.println(theBestCompanyForDeposit.toString());
        } else {
            System.out.println("Лучшей компании для депозита на данный срок не найдено!");
        }
    }

    private static Company researchTheBestCompanyForSendingMoney(Company[] company) {
        Company theBestCompanyForSendingMoney = null;
        double minCommissionForSendingMoney = Double.MAX_VALUE;
        for (Company companyForSendingMoney : company) {
            if (companyForSendingMoney instanceof OperationSendMoney) {
                if (((OperationSendMoney) companyForSendingMoney).sendMoney() < minCommissionForSendingMoney) {
                    theBestCompanyForSendingMoney = companyForSendingMoney;
                    minCommissionForSendingMoney = ((OperationSendMoney) companyForSendingMoney).sendMoney();
                }
            }
        }
        return theBestCompanyForSendingMoney;
    }

    private static void printTheBestCompanyForSendingMoney(Company[] company) {
        Company theBestCompanyForSendingMoney = researchTheBestCompanyForSendingMoney(company);
        if (theBestCompanyForSendingMoney != null) {
            System.out.println(String.format("Лучшей компанией для пересылки данной суммы является: \"%s\"", theBestCompanyForSendingMoney.getName()));
            System.out.println(theBestCompanyForSendingMoney.toString());
        } else {
            System.out.println("Лучшей компании для пересылки данной суммы не найдено!");
        }
    }
}



